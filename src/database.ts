import pg from 'pg'

const {PG_HOST,PG_PORT,PG_USERNAME,PG_PASSWORD,PG_DATABASE,PG_SSL} = process.env

export const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    ssl: PG_SSL ==='enabled'
})

export const executeQuery = async (query:string,parameters?:Array<any>)=>{
    const client = await pool.connect()
    try {
        const result = await client.query(query,parameters)
        return result
    }catch(error:any){
        console.error(error.stack)
        error.name = 'dbError'
        throw error
    }finally{client.release()}
}

// export const createUsersTable =async () => {
//     const query = `CREATE TABLE IF NOT EXISTS "users" (
//         "user_id" SERIAL PRIMARY KEY,
//         "username" VARCHAR(45) NOT NULL,
//         "password" VARCHAR(255) NOT NULL
//     )
//     `
//     await executeQuery(query)
//     console.log("Users table initialized")
// }

// export const createAccountsTable =async () => {
//     const query = `CREATE TABLE IF NOT EXISTS "accounts" (
//         "account_id" SERIAL PRIMARY KEY,
//         "user_id" INTEGER,
//         "balance" INTEGER CHECK (balance >0)
//     )
//     `
//     await executeQuery(query)
//     console.log("Accounts table initialized")
// }