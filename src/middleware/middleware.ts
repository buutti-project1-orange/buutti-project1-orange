import jwt from 'jsonwebtoken'
import { Response, Request, NextFunction } from 'express'

//Checks if endpoint exists

export const endPointNotFound = (req: Request, res: Response) => {
    res.status(404).send({ error: 'oops...wrong place maybe?' })
}

export const reqMustIncludeUsernameAndPass = (req: Request, res: Response, next: NextFunction)=>{
    const { username, password } = req.body
    if(!username || !password)
        return res.status(403).send('Error: Missing username or password in req body')
    next()
}

export const accountMustBePositive = (req:Request,res:Response, next:NextFunction)=>{
    const balance = Number(req.body.balance)
    if (balance <=-0){
        return res.status(404).send('Cannot be negative value')      
    }
    next()
}

export const authenticate = (req: Request, res: Response, next: NextFunction) => {
    const auth = req.get('Authorization')
    if (!auth?.startsWith('Bearer ')) {
        return res.status(401).send('Invalid JWT')
    }
    const token = auth.substring(7)
    const SECRET = process.env.SECRET ?? ''
    try {
        const decodedToken = jwt.verify(token, SECRET)
        console.log(decodedToken)
        
        next()
    } catch (error) {
        return res.status(401).send('Invalid token')
    }
}
