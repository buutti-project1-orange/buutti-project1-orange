import express, { Request, Response } from 'express'
import userRouter from './routes/userRouter'
import accountsRouter from './routes/accountsRouter'
import { endPointNotFound } from './middleware/middleware'
// import {createUsersTable, createAccountsTable} from "./database"

const server = express()
server.use(express.json())

// createUsersTable()
// createAccountsTable()
server.use('/users', userRouter)
server.use('/accounts', accountsRouter)
server.get('/', (req: Request, res: Response) => {
    res.send('JELLO THROUGH PIPELINE (test,lint,deploy)')
})


server.use(endPointNotFound)

export default server
