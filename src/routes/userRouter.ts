import express, { Request, Response } from 'express'
import argon from 'argon2'
import jwt from 'jsonwebtoken'
import { registerUsers, findUserByUSername, modifyPassword, deleteUser, modifyUsername } from '../dao'
import { reqMustIncludeUsernameAndPass } from '../middleware/middleware'
const user = express.Router()

//////POST ROUTER ///////////////////////

// This works
user.post('/register', reqMustIncludeUsernameAndPass, async (req: Request, res: Response) => {
    const { username, password } = req.body
    const userExists = await findUserByUSername(username)
    console.log(userExists)
    

    if (userExists.rows.length === 1) {
        return res.status(401).send('Username already exists')
    }

    const payload = { username: username }
    // add the secret into your .env file, ask joona for the secret
    const secret = process.env.SECRET ?? 'testsecret' 
    const options = { expiresIn: '1h' }
    
    const token = jwt.sign(payload, secret, options)
    const hash = await argon.hash(password)
    
    if (username && hash) {
        registerUsers(username, hash)
        
        return res.status(200).send(token)
    } else {
        res.status(400).send('username or new password hash missing.')
    }

})
// This works
user.post('/login', reqMustIncludeUsernameAndPass, async (req: Request, res: Response) => {
    const { username, password } = req.body
    
    const existingUser = await findUserByUSername(username)

    if (existingUser.rows.length < 1) {
        return res.status(401).send('Username does not exists')
    }

    const passwordHash = existingUser.rows[0].password
    const passwordMatches = await argon.verify(passwordHash, password)
    if (!passwordMatches) {
        return res.status(401).send('Incorrect password!')
    }

    const payload = { username: username }
    const secret = process.env.SECRET ?? '' // add secret to .env, ask joona for secret
    const options = { expiresIn: '1h' }
    
    const token = jwt.sign(payload, secret, options)

    return res.status(200).send(token)
}
  
   
)

// This works
user.patch('/password', async (req: Request, res: Response) => {
    const { username, password } = req.body
    console.log('Logged in')
    
    
    const userExists = await findUserByUSername(username)

    if (userExists.rows.length < 1) {
        res.status(404).send('User does not exist')
    }

    const user_id = userExists.rows[0].user_id
    const newHash = await argon.hash(password)
    await modifyPassword(user_id,newHash)
    return res.status(200).send('Updated password') 
})

user.patch('/username', async (req: Request, res: Response) => {
    const { username, newUsername } = req.body

    const existingUser = await findUserByUSername(username)
    const user_id = existingUser.rows[0].user_id
    
    if (existingUser.rows.length < 1) {
        res.status(404).send('User does not exist')
    }

    const result = await modifyUsername(user_id,newUsername)
    return res.status(201).send(result.rows[0])

})

user.delete('/', async (req: Request, res: Response) => {
    const { username } = req.body
    const result = await deleteUser(username)
    return res.status(201).send(result.rows)
})

export default user
