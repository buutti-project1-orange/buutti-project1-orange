
import express, { Request, Response } from 'express'
import { postAccount, getAllAccounts, deleteAccount, getAccountById, updateAccount } from '../dao'
import { accountMustBePositive } from '../middleware/middleware'

const accounts = express.Router()


// Create a new account


accounts.post('/', accountMustBePositive, async (req: Request, res: Response) => {

    const {balance,user_id} = req.body
    const accountQuantities = await getAllAccounts(user_id)
    if(accountQuantities.rows.length === 3){
        return res.status(400).send('You have reached max amount of accounts.(3)')
    }
    const result = await postAccount(balance,Number(user_id))
    return res.status(201).send(result.rows[0])
})

// Close an existing account

accounts.delete('/:account_id', async (req: Request, res: Response) => {
    const account_id = Number(req.params.account_id)
    const result = await deleteAccount(account_id)
    return res.status(201).send('Acount deleted')
})

// List all user's accounts

accounts.get('/', async (req: Request, res: Response) => {
    const user_id = req.body.user_id
    const result = await getAllAccounts(user_id)
    return res.status(201).send(result.rows)
})

 
// Check account balance

accounts.get('/:account_id', async (req: Request, res: Response) => {
    const account_id = Number(req.params.account_id)
    const result =  (await getAccountById(account_id)).rows[0]
    return res.status(201).send({balance: result.balance})
})

// Withdraw money

accounts.patch('/:account_id/withdraw',async (req: Request, res: Response) => {
    const {amount} = req.body
    const id = Number(req.params.account_id)
    const accountInfo =  await (getAccountById(id))
    const currentBalance = accountInfo.rows[0].balance
    if(accountInfo.rows[0].length < 1){
        res.status(404).send('Account does not exist')
    }
    
    const newBalance =  currentBalance - amount

    if(newBalance < amount){
        return res.status(400).send('user does not have enough funds')
    }
    await updateAccount(id,newBalance)
    return res.status(201).send(await (await (getAccountById(id))).rows)
})

// Deposit money

accounts.patch('/:account_id/deposit', accountMustBePositive,async (req: Request, res: Response) => {

    const {amount} = req.body
    const id = Number(req.params.account_id)
    const accountInfo =  await (getAccountById(id))
    const currentBalance = accountInfo.rows[0].balance

    if(accountInfo.rows[0].length < 1){
        res.status(404).send('account does not exist')
    }
    const newBalance =  currentBalance.balance + amount
    await updateAccount(id,newBalance)
    return res.status(201).send( (await (getAccountById(id))).rows)
  
})


// Transfer money


accounts.patch('/transfer', async (req: Request, res: Response) => {
    const source_account_id = parseInt(req.body.source_account_id)
    const target_account_id = parseInt(req.body.target_account_id)
    const amount_to_transfer = parseInt(req.body.amount_to_transfer)

    // Remove amountToTransfer from account 1
    const sourceAccount = await (getAccountById(source_account_id))
    if(sourceAccount.rows[0].balance < amount_to_transfer) {
        return  res.status(400).send('User does not have enough funds')
    }
    const source_account_balance =  sourceAccount.rows[0].balance - amount_to_transfer
    await updateAccount(source_account_id, source_account_balance)

    // Add amountToTransfer to account 2
    const targetAccount = await (getAccountById(target_account_id))
    const targetAccountBalance =  targetAccount.rows[0].balance + amount_to_transfer
    await updateAccount(target_account_id, targetAccountBalance)

    const sourceAccountLatestVersion = await (getAccountById(source_account_id))
    const sourceAccountNewBalance = await sourceAccountLatestVersion.rows[0].balance
    //! Dao function for withdrawing money from  account by account_id
    
    return res.status(201).send( (await (getAccountById(source_account_id))).rows)

})

export default accounts