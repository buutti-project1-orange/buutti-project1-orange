import server from './server'

const { PORT } = process.env || 3000
server.listen(PORT, () => {
    console.log('Bank Orange API listening to port', PORT) 
})
