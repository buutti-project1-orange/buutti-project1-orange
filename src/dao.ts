import {executeQuery} from './database'

// This works
export const registerUsers = async (username: string,password: string)=>{
    const query = `
    WITH new_user AS (
      INSERT INTO users (username, password)
      VALUES ($1, $2)
      RETURNING user_id
    )
    INSERT INTO accounts (user_id)
    SELECT user_id
    FROM new_user
    ;
  `
    const query2 = `UPDATE accounts
  SET balance=0
  WHERE balance IS NULL;`
    const params = [username, password]
    const result = await executeQuery(query,params)
    await executeQuery(query2)
    return result
}   
// This works
export const findUserByUSername = async (username: string)=>{
    const query = 'SELECT * FROM users WHERE username = $1'
    const params = [username]
    return executeQuery(query,params)
    
}   

export const deleteUser = async (username: string)=>{
    const query = `
    WITH deleted_user AS (
        DELETE FROM users
        WHERE username = $1
        RETURNING user_id
      )
      DELETE FROM accounts
      WHERE user_id IN (SELECT user_id FROM deleted_user);
    `
    const params = [username]
    return  executeQuery(query,params)
    
}   

export const modifyPassword = async (user_id:number,password: string)=>{
    const query = 'UPDATE users SET password = $2 WHERE user_id = $1'
    const params = [user_id,password]
    return  executeQuery(query,params)
    
}   


export const modifyUsername =async (user_id:number,username: string)=>{
    const query = 'UPDATE users SET username = $2 WHERE user_id = $1'
    const params = [user_id,username]
    return  executeQuery(query,params)
    
}   
/////////////Account routers//////////////

export const postAccount = async (balance:number, user_id:number) => {
    const query = 'INSERT INTO accounts(balance, user_id) VALUES ($1, $2) RETURNING account_id' 
    const params = [balance, user_id]
    const result =await executeQuery(query,params)
    return result
}

export const deleteAccount = async(account_id:number)=>{
    const query = 'DELETE FROM accounts WHERE account_id = $1' 
    const params = [account_id]
    return await executeQuery(query,params)
}

export const getAllAccounts = async (user_id:number)=>{
    const query = 'SELECT * FROM accounts WHERE user_id = $1'
    const params= [user_id]
    const result = await executeQuery(query,params)
    console.log(result.rows[0])
    return result
}

export const getAccountById = async (account_id:number) => {
    const query = 'SELECT * FROM accounts WHERE account_id = $1'
    const params = [account_id]
    return await executeQuery(query,params)
}

export const updateAccount = async (account_id: number, amount: number)=>{
    const query = 'UPDATE accounts SET balance = $2 WHERE account_id = $1'
    const params = [account_id, amount]
    return await executeQuery(query,params)
}

