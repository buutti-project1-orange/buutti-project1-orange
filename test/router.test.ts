import server from '../src/server'
import request from 'supertest'
import { jest } from '@jest/globals'
import { pool } from '../src/database'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null,
        }
    })
}

describe('Testing /users/delete', () => {
    const mockResponse = {
        rows: [
            {
                username:'user1'
                
            },
        ],
    }
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    it('Should delete a user by ID', async () => {
        
        const response = await request(server).delete('/users')
        expect(response.status).toBe(201)
        expect(response.body).toStrictEqual([mockResponse.rows[0]])
    })
    it('Should patch a user by ID', async () => {
        
        const response = await request(server).patch('/users/username')
        expect(response.status).toBe(201)
        expect(response.body).toStrictEqual(mockResponse.rows[0])
    })
})
