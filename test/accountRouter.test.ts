import server from '../src/server'
import request from 'supertest'
import { jest } from '@jest/globals'
import { pool } from '../src/database'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null,
        }
    })
}

describe('Testing /accounts', () => {
    const mockResponse = {
        rows: [
            {
                account_id:1,
                user_id:1,
                balance: 200}        
        ],
    }
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    it('Should post new account and return account info', async () => {
        
        const response = await request(server).post('/accounts')
            .send( [{'account_id': 1, 'balance': 200, 'user_id': 1}])
        expect(response.status).toBe(201)
        expect(response.body).toStrictEqual(mockResponse.rows[0])
    })
    
    it('404 status when negative balance amount is posted', async () => {
        
        const response = await request(server).post('/accounts')
            .send( {'account_id': 1, 'balance': -200, 'user_id': 1})
        expect(response.status).toBe(404)
        expect(response.text).toStrictEqual('Cannot be negative value')
    })

    it('delete account by id number', async () => {
        
        const response = await request(server).delete('/accounts/1')
        expect(response.status).toBe(201)
        expect(response.text).toStrictEqual('Acount deleted')
    })
})


describe('Testing post /accounts ', () => {
    const mockResponse = {
        rows: [
            {
                account_id:1,
                user_id:1,
                balance: 200 
            },
            {
                account_id:2,
                user_id:1,
                balance: 200    
            },
            {
                account_id:3,
                user_id:1,
                balance: 200   
            }
        ],
    }
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    
    it('Cannot create new account if usaer already has 3 accounts', async () => {
        
        const response = await request(server).post('/accounts')
            .send(  {'balance': 200, 'user_id': 1})
        expect(response.status).toBe(400)
        expect(response.text).toStrictEqual('You have reached max amount of accounts.(3)')
    })
})


describe('Testing delete /accounts', () => {
    const mockResponse = {
        rows: [
            {
                account_id:1,
                user_id:1,
                balance: 200
            },
        ],
    }
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    

    it('delete account by id number', async () => {
        
        const response = await request(server).delete('/accounts/1')
        expect(response.status).toBe(201)
        expect(response.text).toStrictEqual('Acount deleted')
    })
})


describe('Testing GET /accounts', () => {
    const mockResponse = {
        rows: [
            {
                account_id:1,
                user_id:1,
                balance: 200
            },
            {
                account_id:2,
                user_id:1,
                balance: 200 
            },
            {
                account_id:3,
                user_id:1,
                balance: 200  
            }
        ],
    }
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    

    it('get all accounts by user_id number', async () => {
        
        const response = await request(server).delete('/accounts/1')
        expect(response.status).toBe(201)
        expect(response.text).toStrictEqual('Acount deleted')
    })
})


describe('Testing get /accounts/account_id', () => {
    const mockResponse = {
        rows: [
            {
                account_id:1,
                user_id:1,
                balance: 200
            },
        ],
    }
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    

    it('returns account balance  by id number', async () => {
        
        const response = await request(server).get('/accounts/1')
        expect(response.status).toBe(201)
        expect(response.body).toStrictEqual({balance: 200})
    })
})


describe('Testing patch /accounts/withdraw', () => {
    const mockResponse = {
        rows: [
            {
                account_id:1,
                user_id:1,
                balance: 200
            },
        ],
    }
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    

    it('should withdraw and return new balance', async () => {
        
        const response = await request(server).patch('/accounts/1/withdraw')
            .send({amount: 10})
        expect(response.status).toBe(201)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})


describe('Testing patch /accounts/deposit', () => {
    const mockResponse = {
        rows: [
            {
                account_id:1,
                user_id:1,
                balance: 200
            },
        ],
    }
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    

    it('should deposit money and return new balance', async () => {
        
        const response = await request(server).patch('/accounts/1/deposit')
            .send({amount: 10})
        expect(response.status).toBe(201)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})



describe('Testing patch /accounts/transfer', () => {
    const mockResponse = {
        rows: [
            {
                account_id:1,
                user_id:1,
                balance: 200
            },
            {
                account_id:2,
                user_id:1,
                balance: 0
            }
        ],
    }
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    

    it('should transfer from one account to another and return source account balance', async () => {
        
        const response = await request(server).patch('/accounts/transfer')
            .send({source_account_id: 1,
                target_account_id: 2,
                amount_to_transfer: 100})
        expect(response.status).toBe(201)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

})